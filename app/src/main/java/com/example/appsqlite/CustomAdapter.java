package com.example.appsqlite;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {
    //membuat variable
    private Context context;
    private Activity activity;
    private ArrayList id, judul, deskripsi;

    //function superclass
    CustomAdapter(Activity activity, Context context, ArrayList id, ArrayList judul, ArrayList deskripsi){
        this.activity = activity;
        this.context = context;
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
    }

    @NonNull
    @Override //untuk mengambil layout dasar row.xml
    public CustomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row, parent, false);
        return new MyViewHolder(view);
    }

    @Override //mengisi data atribut textview & memberikan action click pada layout linear layout
    public void onBindViewHolder(@NonNull final CustomAdapter.MyViewHolder holder, final int position) {
        holder.judul.setText(String.valueOf(judul.get(position)));
        holder.deskripsi.setText(String.valueOf(deskripsi.get(position)));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //menampung data judul & deskripsi dalam variable string
                String judulNotes = String.valueOf(judul.get(position));
                String deskNotes = String.valueOf(deskripsi.get(position));

                //bikin dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog);
                TextView Tvnotes, Tvdeskripsi;
                Button Edit, Delete;
                Tvnotes = dialog.findViewById(R.id.judul);
                Tvdeskripsi = dialog.findViewById(R.id.deskripsi);
                Edit = dialog.findViewById(R.id.edit);
                Delete = dialog.findViewById(R.id.delete);
                Tvnotes.setText(judulNotes);
                Tvdeskripsi.setText(deskNotes);
                Edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    Intent intent = new Intent(context, UpdateNotes.class);
                    intent.putExtra("id", String.valueOf(id.get(position)));
                    intent.putExtra("judul", String.valueOf(judul.get(position)));
                    intent.putExtra("deskripsi", String.valueOf(deskripsi.get(position)));
                    activity.startActivityForResult(intent, 1);
                    }
                });
                Delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String idNotes = String.valueOf(id.get(position));
                        DatabaseHalper db = new DatabaseHalper(context);
                        db.deleteOneRaw(idNotes);
                        Intent intent = new Intent(context, MainActivity.class);
                        activity.startActivity(intent);
                    }
                });
                dialog.show();
            }   
        });
    }

    @Override //get array
    public int getItemCount() {
        return id.size();
    }


    //deklarasi kn atribut ke layout xml dengan findViewById
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView judul, deskripsi;
        LinearLayout linearLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.TvJudul);
            deskripsi = itemView.findViewById(R.id.TvDeskripsi);
            linearLayout = itemView.findViewById(R.id.main_layout);


        }
    }


}
