package com.example.appsqlite;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class UpdateNotes extends AppCompatActivity {
    EditText e_judul, e_deksripsi;
    FloatingActionButton FabSave;
    String id, judul, deskripsi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_notes);
        e_judul = findViewById(R.id.EditJudul);
        e_deksripsi = findViewById(R.id.Editdeskripsi);
        FabSave = findViewById(R.id.FABsaveUpdate);
        getData();
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(judul);
        }

        FabSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHalper databaseHalper = new DatabaseHalper(UpdateNotes.this);
                judul = e_judul.getText().toString().trim();
                deskripsi = e_deksripsi.getText().toString().trim();
                databaseHalper.updateData(id, judul, deskripsi);
                Intent intent = new Intent(UpdateNotes.this, MainActivity.class);
                startActivity(intent);
            }
        });



    }

    void getData(){
        if (getIntent().hasExtra("id") && getIntent().hasExtra("judul") && getIntent().hasExtra("deskripsi")){
            //get ada dari kelas MainActivity
            id = getIntent().getStringExtra("id");
            judul = getIntent().getStringExtra("judul");
            deskripsi = getIntent().getStringExtra("deskripsi");

            //set data ke EditText
            e_judul.setText(judul);
            e_deksripsi.setText(deskripsi);
            Log.d("data", "getData: " + id +" "+ judul +" "+ deskripsi);
        }else {
            Toast.makeText(this, "NO data", Toast.LENGTH_SHORT).show();
        }
    }



}