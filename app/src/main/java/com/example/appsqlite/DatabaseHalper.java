package com.example.appsqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

//ini juga bisa di bilang databasenya dalam bentuk class.java
public class DatabaseHalper extends SQLiteOpenHelper {

    //variable context
    private Context context;
    //nama database
    public static final String DATABASE_NAME = "mynotes.db";
    //versi database ini wajib karena ketentuan dari sqlite
    public static final int DATABASE_VERSION = 1;
    //tabel
    public static final String TABLE_NAME = "notes";
    //field/record
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JUDUL = "judul";
    public static final String COLUMN_DEKSRIPSI = "deskripsi";

    //function superclass
    public DatabaseHalper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override //membuat table
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE " + TABLE_NAME +
                        " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        COLUMN_JUDUL + " TEXT, " +
                        COLUMN_DEKSRIPSI + " TEXT );";
        sqLiteDatabase.execSQL(query);
    }

    @Override //drop database apabila aplikasi di hapus
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);

    }

    //method insert
    public void addNotes(String judul, String deskripsi){
        //membuat variable databaase
        SQLiteDatabase db = this.getWritableDatabase();
        //content values digunakan untuk menapung field dalam table
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_JUDUL, judul);
        contentValues.put(COLUMN_DEKSRIPSI, deskripsi);
        double result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1){
            Toast.makeText(context, "Gagal", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "Sukses", Toast.LENGTH_SHORT).show();
        }
    }

    //menampung data jadi array sperti mysql_fetch_array
    Cursor readAllData(){
        String query = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = null;
        if(sqLiteDatabase != null){
            cursor = sqLiteDatabase.rawQuery(query, null);
        }
        return cursor;
    }


    //method update data
    public void updateData(String id, String judul, String deskripsi){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_JUDUL, judul);
        contentValues.put(COLUMN_DEKSRIPSI, deskripsi);
        double result = db.update(TABLE_NAME, contentValues, "id=?", new String[]{id});
        if (result == -1){
            Toast.makeText(context, "Gagal", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "Sukses", Toast.LENGTH_SHORT).show();
        }
    }

    //method delete data
    public void deleteOneRaw(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        double result = db.delete(TABLE_NAME, "id=?", new String[]{id});
        if (result == -1){
            Toast.makeText(context, "Gagal", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "Sukses", Toast.LENGTH_SHORT).show();
        }
    }

    //method delete all data
    public void deleteAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME);
    }






}
