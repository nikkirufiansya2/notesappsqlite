package com.example.appsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class AddNotes extends AppCompatActivity {
    //variable EditText
    EditText judul, deskripsi;
    //Variable FloatingButton
    FloatingActionButton save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);
        judul = findViewById(R.id.EditjudulNotes);
        deskripsi = findViewById(R.id.EditdeskripsiNotes);
        save = findViewById(R.id.simpanFab);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //variable dari class DatabaseHalper
                DatabaseHalper databaseHalper = new DatabaseHalper(AddNotes.this);
                //menjalankan method insert
                databaseHalper.addNotes(judul.getText().toString().trim(), deskripsi.getText().toString().trim());
                Intent intent = new Intent(AddNotes.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}